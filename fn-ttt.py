#!`which python3`
# "Functional" Tic-Tac-Toe

'''
Copyright 2017 Cary Mathews

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

def generateBoard(moves={}):
    board = ([1,2,3],[4,5,6],[7,8,9])
    current = ""
    for row in board:
    	for space in row:
    		if len(moves) > 0:
	    		if space in moves["X"]:
	    			space = "X"
	    		elif space in moves["O"]:
	    			space = "O"
    		current += ("{}|".format(space))
    	current += ("\n")
    	current += ("-+-+-+\n")
    return current


def collectMove(player):
    play = input("Player {}'s move (1-9; 0 to quit): ".format(player))
    while not play.isdecimal():
    	play = input("Player {}'s move (1-9; 0 to quit): ".format(player))
    return int(play)

def appendMove(move, moves):
	m = moves + [move]
	return sorted(m)

def evaluateMove(moves):
	win = ((1,2,3),(4,5,6),(7,8,9),(1,4,7),(2,5,8),(3,6,9),(1,5,9),(3,5,7))
	winGoal = ['+']*3

	for wstate in win:
		pstate = list(wstate)
		for mv in moves:
			pstate = ['+' if x==mv else x for x in pstate]
			if pstate == winGoal:
				return "Win"

	if len(moves) == 5:
		return "Draw"
	elif 0 in moves:
		return "Quit"
	else:
		return "Playing"

def validMove(move, moves):
	if move > 9 or move < 0:		# invalid: move > 9; already played move
		return False
	elif move in moves["X"] + moves["O"]:
		return False
	else:
		return True

def main():
	player = "O"
	moves = {"X":[], "O":[]}
	gameStatus = "Playing"
	while gameStatus == "Playing":
		print(generateBoard(moves))
		player = (lambda p: 'O' if p == 'X' else 'X')(player)
		move = collectMove(player)
		while not validMove(move, moves):
			print(generateBoard(moves))
			print("not a valid move")
			move = collectMove(player)
		moves[player] = appendMove(move, moves[player])
		gameStatus = evaluateMove(moves[player])

	if gameStatus == "Win":
		print("Congratulations, player {}, you win!".format(player))
	elif gameStatus == "Quit":
		print("Thanks for playing!")
		exit()
	else:
		print("A Draw! Try Again!")

main()
# TicTacToe

These are simple tic-tac-toe programs I've written for the purpose of exploring a programming language. I'm not a professional programmer, but anyone is welcome to use these for any purpose under the MIT License.